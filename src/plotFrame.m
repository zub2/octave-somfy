## -*- texinfo -*-
## @deftypefn {Function File} plotFrame (@var{y},n@var{offsets})
## Plot annotated frame. @var{y} is the demodulated OOK signal and
## @var{offsets} are the somfy frame offsets as returned by findSomfy.
##
## @seealso{findSomfy}
## @seealso{makeTransitionsPolyline}
##
## @end deftypefn

function plotSignal(y, offsets)
  narginchk(2, 2);

  if rows(offsets) == 0
    error('offsets argument is empty - no frames detected?');
  endif

  l = makeTransitionsPolyline(y);

  clf;
  title('Somfy frame(s)');
  plot(l(:,1), l(:, 2));
  ylim([-1,2]);

  function vertical_line(x, label)
    line([x, x], [-0.1, 1.1], "color", "r");
    text(x, -0.1, label);
  endfunction

  for y = 1:rows(offsets)
    row = offsets(y, :);

    header_start = row(1);
    header_end = row(2);
    frame_end = row(3);

    vertical_line(header_start, "hdr start");
    vertical_line(header_end, "hdr end");
    vertical_line(frame_end, "end");
  endfor
endfunction

