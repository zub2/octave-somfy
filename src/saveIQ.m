## -*- texinfo -*-
## @deftypefn {Function File} saveIQ (@var{z}, @var{fname})
## Store analytic signal into an IQ file @var{fname}.
##
## The IQ file is a binary file that consits of pairs of 8 bit unsigned
## integers where each pair are the real and complex parts of a sample of the
## analytic signal.  Such file is produced e.g. by rtl_sdr.
##
## The analytic signal is split into real and imaginary parts and converted
## to 8-bit unsigned integer (0..255). The input range is expected to be
## [-1, +1] (or more precisely: [-1, +127/128]).
##
## @seealso{loadIQ}
## @end deftypefn

function saveIQ(z, fname)
  narginchk(2, 2);

  f = fopen(fname, "wb");
  if f < 0
    error("Can't open file '%s' for writing!", fname);
  endif

  % split I + i*Q into pairs (I, Q)
  z = [real(z(:)), imag(z(:))]'(:);

  % map from [-1, 127/128] to [0, 255] (invert what loadIQ does)
  z = 128*(z + 1);

  fwrite(f, z, "uint8");

  fclose(f);
endfunction
