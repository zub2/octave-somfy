## -*- texinfo -*-
## @deftypefn {Function File} findAndDecodeFrames (@var{y})
## @deftypefnx {Function File} findAndDecodeFrames (@var{y}, @var{verbose})
## Function that scans demodulated signal @var{y} for all somfy frames, decodes
## and prints them.
##
## The optional parameter @var{verbose} can be used to increase verbosity.
## Defaults to 0.
##
## @seealso{findSomfy}
## @seealso{cutSignal}
## @seealso{decodeSomfy}
## @seealso{printSomfyFrame}
##
## @end deftypefn

function findAndDecodeFrames(y, verbose)
  narginchk(1, 2);

  if nargin == 1
    verbose = 0;
  end

  somfyFrames = findSomfy(y, verbose);

  fprintf("found %d frame(s)\n", numel(somfyFrames));

  for frameIdx = 1:numel(somfyFrames)
    somfyFrame = somfyFrames(frameIdx);

    if frameIdx != 1
      fprintf("\n");
    endif

    fprintf("Frame #%d (%s %s):\n", frameIdx, somfyFrame.remoteType, somfyFrame.frameType);
    frame = decodeSomfy(cutSignal(y, somfyFrame.payloadStart, somfyFrame.frameEnd), somfyFrame.remoteType, verbose);
    printSomfyFrame(frame);
  end
endfunction
