## -*- texinfo -*-
## @deftypefn {Function File} {@var{bytes} =} bitsToBytes (@var{bits})
## Assemble an array of individual bits into an array of bytes.
##
## @end deftypefn

function bytes = bitsToBytes(bits)
  bytes = [];
  for i = 1:numel(bits)
    bit = bits(i);
    bitWeight = 7 - mod(i-1, 8);
    if bitWeight == 7
      bytes = [bytes, 0];
    end
    bytes(numel(bytes)) += bit * 2^bitWeight;
  end
endfunction
