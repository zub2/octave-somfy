## -*- texinfo -*-
## @deftypefn {Function File} {@var{offsets} =} findSomfy (@var{y})
## @deftypefnx {Function File} {@var{offsets} =} findSomfy (@var{y}, @var{verbose})
## Find somfy frames in signal @var{y} and return a structure array an element
## for each detected Somfy frame. Each element contains the following fields:
##
## @enumerate
## @item frameStart - timestamp of the start of frame
## @item payloadStart - timestamp of start of paylod
## @item frameEnd - timestamp of the end of the frame (=end of payload)
## @item remoteType - type of remote, one of Telis and Keytis
## @item frameType - type of frame, one of normal and repeat
## @end enumerate
##
## The time of frame start and payload end coincides with a transition while
## the time of payload start can lie between transition (this is because the
## header ends after a fixed time at level 0 and immediately frame starts,
## possible at the same level). Use cutSignal to extract the relevant parts.
## As the function does not decode the payload, it never includes a trailing 0
## at the frame end, even if it is actually there. This needs to be handled
## in frame decoding (i.e. the decoder needs to assume a trailing 0 is there if
## it is needed to make the frame decodable).
##
## The optional parameter @var{verbose} can be used to increase verbosity.
## Defaults to 0.
##
## @seealso{cutSignal}
## @seealso{decodeSomfy}
## @seealso{getSomfyFrameTypeName}
##
## @end deftypefn

function offsets = findSomfy(y, verbose)
  narginchk(1, 2);

  if nargin == 1
    verbose = 0;
  end

  HW_SYNC = [
    2.47e-3, % hw sync (1)
    2.55e-3  % hw sync (0)
  ];

  % Telis ordinary frame
  % (durations in seconds of expected 1s and 0s)
  TELIS_FRAME_START = [
    10.4e-3, % wakeup (1)
    7.1e-3,  % wakeup (0)
    repmat(HW_SYNC, 2, 1),
    4.8e-3,  % sw sync (1)
    645e-6   % sw sync (0)
  ];

  % Telis repeat frame
  TELIS_REPEAT_FRAME_START = [
    repmat(HW_SYNC, 7, 1),
    4.8e-3,  % sw sync (1)
    645e-6   % sw sync (0)
  ];

  % Keytis ordinary frame
  KEYTIS_FRAME_START = [
    repmat([
      2.5e-3 % 1
      2.6e-3 % 0
    ], 12, 1),
    4.8e-3,  % sw sync (1)
    645e-6   % sw sync (0)
  ];

  % Keytis repeat frame
  KEYTIS_REPEAT_FRAME_START = [
    repmat([
      2.5e-3 % 1
      2.6e-3 % 0
    ], 6, 1),
    4.8e-3,  % sw sync (1)
    645e-6   % sw sync (0)
  ];

  EXPECTED(1).start = TELIS_FRAME_START;
  EXPECTED(1).remoteType = 'Telis';
  EXPECTED(1).frameType = 'normal';

  EXPECTED(2).start = TELIS_REPEAT_FRAME_START;
  EXPECTED(2).remoteType = 'Telis';
  EXPECTED(2).frameType = 'repeat';

  EXPECTED(3).start = KEYTIS_FRAME_START;
  EXPECTED(3).remoteType = 'Keytis';
  EXPECTED(3).frameType = 'normal';

  EXPECTED(4).start = KEYTIS_REPEAT_FRAME_START;
  EXPECTED(4).remoteType = 'Keytis';
  EXPECTED(4).frameType = 'repeat';

  % tolerance (for a match, abs(expected - actual) < TOLERANCE*expected)
  TOLERANCE = 0.1;

  % minimal duration of a 0 after a frame is finished
  % findSomfy doesn't decode the bits, it only assumes a frame ends after
  % MIN_SILENCE_AFTER
  MIN_SILENCE_AFTER = 1.9e-3;

  function appendResult(idxStart, matchLen, headerDeltaT, idxEnd, seqIdx)
    if not(exist('offsets'))
      offsetsIdx = 1;
    else
      offsetsIdx = numel(offsets) + 1;
    endif

    offsets(offsetsIdx).frameStart = y(idxStart, 1);
    offsets(offsetsIdx).payloadStart = y(idxStart + matchLen - 1, 1) + headerDeltaT;
    offsets(offsetsIdx).frameEnd = y(idxEnd, 1);
    offsets(offsetsIdx).remoteType = EXPECTED(seqIdx).remoteType;
    offsets(offsetsIdx).frameType = EXPECTED(seqIdx).frameType;
  endfunction

  function b = durationMatches(actual, expected)
    b = abs(actual - expected) < TOLERANCE * expected;
  endfunction

  function b = durationMatchesAtLeast(actual, expected)
    b = actual - expected > -TOLERANCE * expected;
  endfunction

  function sequenceIdx = matchSequenceStart(duration, sequenceIdxStart)
    sequenceIdx = sequenceIdxStart;
    while sequenceIdx <= numel(EXPECTED) && ~durationMatches(duration, EXPECTED(sequenceIdx).start(1))
      sequenceIdx++;
    end
    if sequenceIdx > numel(EXPECTED)
      sequenceIdx = NaN;
    end
  endfunction

  seqIdx = NaN;
  matchStartIdx = NaN;
  matchLen = 0;
  idx = 1;
  while idx < rows(y)
    if (verbose >= 3)
      fprintf("idx=%d\n", idx);
    endif
    state = y(idx, 2);
    ts_start = y(idx, 1);
    ts_end = y(idx + 1, 1);
    duration = ts_end - ts_start;

    if (isnan(matchStartIdx))
      % don't have sequence start
      if (state == 1)
        % try to match any of the sequences
        seqIdx = matchSequenceStart(duration, 1);
        if (~isnan(seqIdx))
          % found a possible sequence start
          if (verbose >= 1)
            fprintf("found possible sequence #%d start at offset %d (TS %f-%f)\n", seqIdx, idx, ts_start, ts_end);
          end
          matchStartIdx = idx;
          matchLen = 1;
        end
      end
      % else: don't care
      idx++;
    elseif (matchLen < numel(EXPECTED(seqIdx).start))
      % at least 1 element in sequence seqIdx is matched
      % assume the EXPECTED sequences start with 1 and then alternate
      expectedState = ((-1)^matchLen + 1)/2;

      if (state == expectedState && matchLen+1 < numel(EXPECTED(seqIdx).start) &&
          durationMatches(duration, EXPECTED(seqIdx).start(matchLen+1)))
        % next matched
        if verbose >= 2
          fprintf("matched #%d element %d (duration %f, state %d) at index %d (TS %f-%f)\n", seqIdx, matchLen+1, EXPECTED(seqIdx).start(matchLen+1), expectedState, idx, ts_start, ts_end);
        end
        matchLen++;
        idx++;
      elseif (state == expectedState && matchLen+1 == numel(EXPECTED(seqIdx).start) &&
          durationMatchesAtLeast(duration, EXPECTED(seqIdx).start(end)))
        if (durationMatches(duration, EXPECTED(seqIdx).start(end)))
          headerDeltaT = 0;
        else
          headerDeltaT = max([0, duration - EXPECTED(seqIdx).start(end)]);
        end
        if (verbose >= 1)
          fprintf("sequence #%d matched (ends at idx %d, state %d, TS %f-%f), searching for frame end...\n", seqIdx, idx, expectedState, ts_start, ts_start + headerDeltaT);
        end
        matchLen++;
        idx++;
      else
        % doesn't match, try remaining sequences
        if (verbose >= 1)
          fprintf("giving up on sequence #%d at offset %d (TS=%f-%f, expectedState=%d, actualState=%d, expectedDuration=%f, actualDuration=%f, elements matched=%d)\n",
            seqIdx, idx, ts_start, ts_end, expectedState, state, EXPECTED(seqIdx).start(matchLen+1), duration, matchLen);
        end
        startDuration = y(matchStartIdx + 1, 1) - y(matchStartIdx, 1);
        seqIdx = matchSequenceStart(startDuration, seqIdx + 1);
        if (~isnan(seqIdx))
          matchLen = 1;
          idx = matchStartIdx + 1;
          if (verbose >= 1)
            fprintf("re-trying with sequence #%d start at offset %d (TS %f-%f) (1st element matches)\n", seqIdx, matchStartIdx, y(matchStartIdx, 1), y(matchStartIdx+1, 1));
          end
        else
          % nothing matches, reset and try next
          idx = matchStartIdx + 1;
          matchStartIdx = NaN;
          matchLen = 0;
        end
      end
    else
      % all elements matched, searching for the end
      if (state == 0 && duration >= MIN_SILENCE_AFTER)
        % hooray, got a match
        if (verbose >= 1)
          fprintf("sequence #%d end matched at offset %d (TS %f-%f)\n", seqIdx, idx, ts_start, ts_end);
        end
        appendResult(matchStartIdx, matchLen, headerDeltaT, idx, seqIdx);

        % search for next...
        matchStartIdx = NaN;
        seqIdx = NaN;
        matchLen = 0;
      end

      idx++;
    end
  end

  % if a frame beginning was found and end of data was reached while searching
  % for frame end, assume that is the end of the frame
  % This is a bit dodgy, but it's useful for pre-trimmed data.
  if (~isnan(matchStartIdx) && matchLen == numel(EXPECTED(seqIdx).start))
    fprintf("WARNING: end of data reached while searching for frame end, assuming frame ends there.\n");
    appendResult(matchStartIdx, matchLen, headerDeltaT, idx, seqIdx);
  end

endfunction
