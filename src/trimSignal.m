## -*- texinfo -*-
## @deftypefn {Function File} {@var{y_trimmed} =} trimSignal (@var{y})
## Trim inactive period at the beginning and end of a signal. Optionally
## specify duration of the initial 0 level at both ends (this makes for
## nicer graphs).
##
## @seeAlso{ookDemodulate}
## @seealso{plotSignal}
##
## @end deftypefn

function y_trimmed = trimSignal(y)
  narginchk(1, 1) % TODO: extend

  y_trimmed = y;

  % trim the beginning
  if y_trimmed(1,2) == 0
    offset = y_trimmed(2,1);
    y_trimmed(1,:) = [];
    y_trimmed(:,1) -= offset;
  endif

  % trim the end
  if y_trimmed(end,2) == 0
    y_trimmed(end,:) = [];
  endif
endfunction
