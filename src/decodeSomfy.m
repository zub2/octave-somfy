## -*- texinfo -*-
## @deftypefn {Function File} {@var{frame} =} decodeSomfy (@var{y}, @var{remoteType})
## @deftypefnx {Function File} {@var{frame} =} decodeSomfy (@var{y}, @var{remoteType}, @var{verbose})
## Decode a somy frame from its signal representation @var{y} and @var{remoteType}
## and return a structure containing the decoded payload.
##
## The signal @var{y} is an n-by-2 matrix, typically an output of cropSignal
## applied to indices found via findSomfy.
##
## @var{remoteType} is one of 'Telis' or 'Keytis'.
##
## This is based on https://pushstack.wordpress.com/somfy-rts-protocol/ (for Telis)
## but the timing seems to be different for my somfy.  Also, i tried to make the
## manchester decoding as robust as possible without depending on particular
## clock frequency.  This is unnecessarily complex but in the beginning I didn't
## trust the timing.
##
## Keytis support has been added with the help of Mariusz Białończyk.
##
## The optional parameter @var{verbose} can be used to increase verbosity.
## Defaults to 0.
##
## @seealso{findSomfy}
## @seealso{cutSignal}
## @seealso{printSomfyFrame}
##
## @end deftypefn

function frame = decodeSomfy(y, remoteType, verbose)
  narginchk(2, 3);

  if nargin == 2
    verbose = 0;
  endif

  % use k-means clustering just for the "fun"... everything seems too fuzzy
  durations = calculateDurations(y);
  [clusterIdx, clusterCenters] = kmeans(durations(:,1), 2);
  [~, shortClusterIdx] = min(clusterCenters);
  [~, longClusterIdx] = max(clusterCenters);

  if verbose >= 1
    fprintf("estimated half-symbol width: %fµs\n", clusterCenters(shortClusterIdx)*1e6);
    fprintf("estimated symbol width: %fµs\n", clusterCenters(longClusterIdx)*1e6);
    fprintf("full/half ratio (should be close to 2): %f\n", clusterCenters(longClusterIdx)/clusterCenters(shortClusterIdx));
  endif

  shortClusterIndices = clusterIdx == shortClusterIdx;
  longClusterIndices = clusterIdx == longClusterIdx;

  maxShortCluster = max(durations(shortClusterIndices));
  minLongCluster = min(durations(longClusterIndices));

  % mid point between the clusters
  LONG_THRESHOLD = (maxShortCluster + minLongCluster) / 2;

  fprintf("using treshold %fµs\n", LONG_THRESHOLD*1e6);

  % decode manchester encoding
  if durations(1, 1) > LONG_THRESHOLD
    error("the first symbol is not a half-width symbol!");
  end

  expectingHalfSym = false;
  bits = [~durations(1,2)];
  if verbose >= 2
    fprintf("initial half-sym: value=%d, bit=%d\n", durations(1, 2), bits(1));
  endif

  for i = 2:rows(durations)
    d = durations(i, 1);
    v = durations(i, 2);
    if verbose >= 2
      fprintf("transition #%d: duration=%f, value=%d, abs time=%f-%f\n", i, d, v, y(i, 1), y(i+1, 1));
    endif

    if expectingHalfSym
      if d > LONG_THRESHOLD
        error("expecting a half-symbol but got a full symbol at index %d (out of %d)\n", i, rows(durations));
      end
      bits = [bits, ~v];
      expectingHalfSym = false;
      if verbose >= 2
        fprintf("got half symbol as expected, appending bit %d\n", bits(end));
      endif
    elseif d > LONG_THRESHOLD
      % full-width symbol -> new bit = !prev bit
      bits = [bits, ~v];
      if verbose >= 2
        fprintf("got full symbol, appending bit %d\n", bits(end));
      endif
    else % d <= LONG_THRESHOLD
      % half-width symbol
      expectingHalfSym = true;
      if verbose >= 2
        fprintf("got initial half-width symbol, waiting for 2nd half-width symbol\n");
      endif
    end
  end

  % The frame (as detected by findSomfy) will lack a trailing 0 (if the frame
  % actually ends with a 0). If this is so, silently assume trailing 0 (the way
  % the decoding above happens - values are added on he initial half-symbol -
  % means nothing really needs to happen here, the 0 has already been appended.
  % So just suppress any warning if it's a 0 we're missing.
  if !expectingHalfSym && v != 1
    warning("unexpected: data ends with a full-width symbol (half-width symbol expected)");
  endif

  if verbose >= 1
    % print raw received bits
    fprintf("raw decoded bits: %s\n", stringifyBits(bits));
  end

  TELIS_BITS = 56;
  switch (remoteType)
    case 'Telis'
      EXPECTED_BITS = TELIS_BITS;
    case 'Keytis'
      EXPECTED_BITS = 80;
    otherwise
      error('Unknown remote type "%s"!', remoteType);
  endswitch

  if numel(bits) ~= EXPECTED_BITS
    warning("decoded unexpected number of bits: expected %d, got %d", EXPECTED_BITS, numel(bits));
  end

  % gather into bytes
  rawBytes = bitsToBytes(bits);

  if verbose >= 1
    % print raw received bytes
    fprintf("raw decoded bytes: %s\n", stringifyBytes(rawBytes));
  end

  % deobfuscate
  % FIXME: Keytis - it seems the frame is longer (80 bits vs 56). The 56 bits
  % seem to use the same obfuscation and have similar meaning like Telis frames
  % with the exception that the control code is always 0xf and actual button
  % info must be encoded somewhere in the remaining 24 bits. I don't know if
  % these use the same obfuscation mechanism. Let's assuem so for the moment.
  bytes = rawBytes;
  for i = numel(bytes):-1:2
    bytes(i) = bitxor(bytes(i), bytes(i-1));
  end

  % decode fields
  frame.remoteType = remoteType;
  frame.rawBits = bits;
  frame.rawBytes = rawBytes;
  frame.bytes = bytes;

  decoded = cell();
  decoded.key = bytes(1);
  decoded.ctrl = bitshift(bytes(2), -4);
  decoded.ctrlName = getSomfyCtrlName(decoded.ctrl);
  decoded.checksum = bitand(bytes(2), 0xf);
  decoded.rollingCode = bitshift(bytes(3), 8) + bytes(4);
  decoded.address = bitshift(bytes(5), 2*8) + bitshift(bytes(6), 8) + bytes(7);

  if strcmp(remoteType, 'Keytis')
    % store extra Keytis data
    % currently just raw and tentatively deobfuscated data
    frame.keytisRawExtraBits = bits(TELIS_BITS + 1:end);
    frame.keytisExtraBytes = bytes(TELIS_BITS/8 + 1:end);
  endif

  frame.decoded = decoded;
endfunction

function name = getSomfyCtrlName(ctrlValue)
  switch ctrlValue
    case 1
      name = "my";
    case 2
      name = "up";
    case 3
      name = "my+up";
    case 4
      name = "down";
    case 5
      name = "my+down";
    case 6
      name = "up+down";
    case 8
      name = "prog";
    case 9
      name = "sun+flag";
    case 10
      name = "flag";
    case 15
      name = "unknown (Keytis default value?)";
    otherwise
      name = "unknown";
  endswitch
endfunction
