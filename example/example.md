# Example Decoding of a RTS Frame

This example uses a file captured by rtl_sdr using the `record-dat.sh` script.

## Initial Setup

To use the Octave functions provided by octave-somfy, you will need:
* Octave (surprise! :) ), tested with Octave 8.3.0
* signal and statistics Octave packages (Debian and Ubuntu package these as `octave-signal` and `octave-statistics`).

You will need a recording of a broadcast of a Somfy RTS remote. The formats these functions accept are described in [top-level README](../README.md). For the purpose of this example, an `rtl_sdr` capture file will be used. The file is compressed using `lzip`, so get `lzip` and unpack the file first:
```bash
cd example
lzip -dk example.dat.lz
```
This should create `example.dat`.

## Decoding `example.dat`

1. Start octave and make the octave-somfy functions available:
   ```matlab
   cd path/to/where/somfy.m-is % or just navigate to the directory in the File Browser and choose Set Current Directory in the right-click menu
   run("somfy.m");
   ```
2. Decode the file:
   ```matlab
   loadAndDecodeIQ('example/example.dat');
   ```
   This should result in (note that `ookDemodulate` is slow and can take >10s even for this short capture file):
   ```
   Running ookDemodulate...
   ookDemodulate finished
   found 1 frame(s)
   Frame #1 (Telis normal):
   using treshold 967.500000µs
   remote type: Telis
   raw bits: 1010.1010|1000.0101|1000.0000|1110.0001|1101.0011|1011.1010|1111.1111
   raw bytes: AA 85 80 E1 D3 BA FF
   deobfuscated bytes: AA 2F 05 61 32 69 45
   key: 0xaa
   ctrl: 0x2 (up)
   checksum: 0xf
   rolling code: 0x0561
   address: 0x326945
   ```
   This shows the high-level function which does all the steps.
3. Perform the same using more low-level functions: Load the `dat` (IQ) file:
   ```matlab
   z=loadIQ('example/example.dat');
   ```
4. Plot the absolute value of the signal:
   ```matlab
   plot(abs(z));
   ```
   You should see something like the following:
   ![image plot(abs(z))](images/abs-z.png)
   This shows the OOK signal is present and likely can be demodulated.
5. Demodulate the OOK signal (note that the second argument must match the sample rate used when capturing the data; `../record-dat.sh` and `loadAndDecodeIQ` assume it's 2.6*10<sup>6</sup>; the example file is also using this sample rate):
   ```matlab
   y=ookDemodulate(z, 2.6e6);
   ```
6. Plot the demodulated signal:
   ```matlab
   plotSignal(y);
   ```
   The graph - after zooming into the part with the actual data - should look like this:
   ![image plotSignal(y)](images/signal-zoomed.png)
   This shows the RTS header (starting with the HW sync part, followed by a normal frame header and the actual payload).
7. Find somfy frame(s) (this capture containes just a single RTS frame):
   ```matlab
   somfyFrames=findSomfy(y, 1); % passing 1 as the second argument makes the function more verbose
   ```
8. Get the actual payload of the first (and only) frame and plot it:
   ```matlab
   payload=cutSignal(y, somfyFrames(1).payloadStart, somfyFrames(1).frameEnd);
   plotSignal(payload)
   ```
   This (after some panning) should show the following graph:
   ![image plotSignal(payload)](images/trimmed-payload.png)
9. Decode the payload:
   ```matlab
   frame = decodeSomfy(payload, somfyFrames(1).remoteType, 1); % 1 to make the function verbose
   ```
10. Dump the decoded frame:
   ```matlab
   printSomfyFrame(frame);
   ```
   This should result in:
   ```
   remote type: Telis
   raw bits: 1010.1010|1000.0101|1000.0000|1110.0001|1101.0011|1011.1010|1111.1111
   raw bytes: AA 85 80 E1 D3 BA FF
   deobfuscated bytes: AA 2F 05 61 32 69 45
   key: 0xaa
   ctrl: 0x2 (up)
   checksum: 0xf
   rolling code: 0x0561
   address: 0x326945
   ```
